/*******************************************************************************
**
**  Cellular Automaton Class
**
*******************************************************************************/

class CA{
  ArrayList<Cell> cells;   // variable length array of cells: road
  int generation;          // time step
  int scl;                 // cell size
  int num;                 // the number of cells: how many cells the road has
  float probability;       // the probability of car existence at the initial step
  
  CA(){
    scl = 1;
    probability = 0.4;
    cells = new ArrayList<Cell>();
    num = int(width/scl);
    restart();
  }
  
  /**********************
  *
  *  restart function: restart the cellular automaton
  *
  **********************/
  void restart(){
    for(int i = 0; i < num; ++i){
      cells.add(new Cell(probability));
    }
    generation = 0;
  }
  
  
  //  The process of creating the new generation
  void generate(){
    for(int i = 1; i < num+1; ++i){
      Cell meLeft   = cells.get((i-1)%num);
      Cell me       = cells.get(i%num);
      Cell meRight  = cells.get((i+1)%num);
      me.Rule(meLeft.exist, meRight.exist, meLeft.stop);
    }

    //  Cells update
    for(Cell c: cells){
      c.update();
    }
    generation++;
  }
  
  /**********************
  *
  *  OUTPUT
  *    display function: display the traffic on the screen
  *
  *    outputString function: output the sequence of cars by string
  *      @return    string of car sequence    
  *
  **********************/
  void display(){
    for(int i = 0; i < num; ++i){
      Cell c = cells.get(i);
      if(c.exist){
        fill(0);  // black if the cell has a car
      }else{
        fill(255);  // white if the cell has no car
      }
      noStroke();
      rect(i*scl, generation*scl, scl, scl);
    }
  }
  
  String outputString(){
    String output = new String();
    for(Cell c: cells){
      if(c.exist){
        output += "1, ";
      }else{
        output += "0, ";
      }
    }
    return output;
  }
  
  //  end the calculation
  boolean finished(){
    if(generation > height/scl){
      return true;
    }else{
      return false;
    }
  }
};