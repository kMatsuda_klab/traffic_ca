//  include to use arraylist and iterator
import java.util.ArrayList;
import java.util.Iterator;


//  initiation of cellullar automaton and output
CA ca;
PrintWriter output;
int count;

void setup(){
  size(1000, 800);
  ca = new CA();
  background(255);
  count = 1;
  
  output = createWriter("Traffic" + count + ".csv");
}

void draw(){
  ca.display();
  ca.generate();
  
  output.print(ca.outputString());
  output.print("\n");
  
  if(ca.finished()){
    output.flush();
    output.close();
    saveFrame("Traffic" + count + ".jpg");
    
    count++;
    if(count <= 5){
      output = createWriter("Traffic" + count + ".csv");
      ca.restart();
    }else{
      exit();
    }
    
    background(255);
  }
}

void mousePressed(){
  background(0);
  ca.restart();
}