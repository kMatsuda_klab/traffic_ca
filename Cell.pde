/*******************************************************************************
*
*  Cell Class
*    have two-states (car exists or not)
*    have information about the speed of the car (stop or not)
*
*******************************************************************************/

class Cell{
  boolean exist;  // there is a car at the current step
  boolean stop;  // stop flag: the car stopped at the former step
  boolean existNext;  // there is a car at the next step
  boolean stopNext;  // stop flag: the car is stopping at the current step
  
  
  /**********************
  *
  *  Initiator  
  *    @param    float probability      : the cell has a car at the initial step
  *    
  **********************/
  Cell(float probability){
    if(probability > random(1)){
      exist = true;
    }else{
      exist = false;
    }
    
    stop = false;
    existNext = false;
    stopNext = false;
  }
  
  
  //  Cell update
  void update(){
    exist = existNext;
    stop = stopNext;
    existNext = false;
    stopNext = false;
  }
  
  /**********************
  *
  *  Rule function: determine the state of the next step  
  *    @param    boolean left      : the left cell has a car
  *    @param    boolean right     : the right cell has a car
  *    @param    boolean leftstep  : the car of left cell stopped at the former step
  *
  **********************/
  void Rule(boolean left, boolean right, boolean leftstop){
    //  judge whether the car stops or not at the next step
    if(exist && right){
      stopNext = true;
    }else{
      stopNext = false;
    }
    
    //  judge whether the cell has a car or not at the next step
    if(exist){
      
      if(stop || right){
        existNext = true;
      }else{
        existNext = false; 
      }
    }else{
      if(!left || leftstop){
       existNext = false; 
      }else{
        existNext = true;
      }
    }  
  }
};